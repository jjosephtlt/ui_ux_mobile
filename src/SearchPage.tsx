import {Image, StyleSheet, Text, TextInput, View} from 'react-native';
import React from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SearchResult from './SearchResult';

interface SearchPageProps {}

const SearchPage: React.FC<SearchPageProps> = ({}) => {
  return (
    <View style={{padding: 20, backgroundColor: 'white', height: '100%'}}>
      <View style={{alignItems: 'flex-end'}}>
        <View style={styles.toggle}>
          <View style={styles.fill_color}>
            <Text style={{color:'white'}}>Delivery</Text>
          </View>

          <View style={{padding: 10}}></View>
          <Text style={{color:'black'}}>Take Away</Text>
        </View>
        <View style={{padding: 10}}></View>
      </View>
      <View style={styles.search_container}>
        <View style={{flexDirection: 'row'}}>
          <MaterialCommunityIcons
            style={{paddingHorizontal: 10}}
            name="home"
            size={30}
            color="blue"
          />
          <TextInput
            style={styles.input}
            placeholder="Search for dishes and restaurants "
            keyboardType="numeric"
          />
          <View style={styles.partitionLineVertical} />
          <MaterialCommunityIcons
            style={{paddingHorizontal: 10}}
            name="home"
            size={30}
            color="blue"
          />
        </View>
      </View>
      <View style={{padding: 10}}></View>

      <SearchResult></SearchResult>
      <SearchResult></SearchResult>
      <SearchResult></SearchResult>

      <SearchResult></SearchResult>
    </View>
  );
};

export default SearchPage;

const styles = StyleSheet.create({
  toggle: {
    borderRadius: 30,
    borderWidth: 1,
    borderColor: '#EE7A57',
    width: '50%',
    height: 35,
    flexDirection: 'row',
    alignItems: 'center',
  },
  fill_color: {
    backgroundColor: '#EE7A57',
    height: 35,
    width: '45%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
  },
  search_container: {
    height: 50,
    backgroundColor: 'white',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 1.5,
    shadowRadius: 7,
    elevation: 5,
  },
  input: {
    height: 50,
    width: '75%',

    padding: 10,

    backgroundColor: 'white',
    color: '#D8D8D8',
  },
  partitionLineVertical: {
    width: 0.7,
    height: '50%',
    backgroundColor: 'black',
    marginHorizontal: 10,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
