import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';

interface SearchResultProps {}

const SearchResult: React.FC<SearchResultProps> = ({}) => {
  return (
    <View style={{flexDirection: 'row'}}>
      <View style={styles.circle}>
        <Image
          source={require('../assets/icecreame.jpg')}
          style={styles.image}
          resizeMode="cover"
        />
      </View>
      <View style={{padding: 5}}></View>
      <View style={{justifyContent: 'center'}}>
        <Text style={styles.bold_text}>Ice Cream</Text>
        <Text style={{fontSize: 10}}>Dish</Text>
      </View>
    </View>
  );
};

export default SearchResult;

const styles = StyleSheet.create({
  circle: {
    backgroundColor: 'red',
    height: 50,
    width: 50,
    borderRadius: 40,
    margin: 5,
    overflow: 'hidden',
  },
  image: {height: '100%', width: '100%'},
  bold_text: {color: 'black', fontWeight: '600'},
});
