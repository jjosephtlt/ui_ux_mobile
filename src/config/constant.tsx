export enum RoutPath {
  homePage = 'home',
  loginPage = 'login',
  tabNavigationPage = 'tabNavigation',
  deliveryPage = 'delivery',
  amount = 'amount',
  takeAway = 'takeAway',
  launch = 'launch',
  search = 'search',
}
const Dishes = [
  {id: '1', title: 'Biriyani'},
  {id: '2', title: 'Shavarma'},
  {id: '3', title: 'Dosa'},
  {id: '4', title: 'Perotta'},
  {id: '5', title: 'Edali'},
  {id: '6', title: 'Poori'},
  {id: '7', title: 'Edali'},
  {id: '8', title: 'Poori'},
];
const Restaurant = [
  {id: '1', title: 'Filter'},
  {id: '2', title: 'Sort'},
  {id: '3', title: 'Pure veg'},
  {id: '4', title: 'Offer'},
  {id: '5', title: 'Fast delivery'},
];
