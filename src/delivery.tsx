import {
  FlatList,
  Image,
  Pressable,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {useNavigation} from '@react-navigation/native';
import {RoutPath} from './config/constant';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

interface DeliveryProps {}

const Delivery: React.FC<DeliveryProps> = ({}) => {
  const Dishes = [
    {id: '1', title: 'Biriyani'},
    {id: '2', title: 'Shavarma'},
    {id: '3', title: 'Dosa'},
    {id: '4', title: 'Perotta'},
    {id: '5', title: 'Edali'},
    {id: '6', title: 'Poori'},
    {id: '7', title: 'Edali'},
    {id: '8', title: 'Poori'},
  ];
  const Restaurant = [
    {id: '1', title: 'Filter'},
    {id: '2', title: 'Sort'},
    {id: '3', title: 'Pure veg'},
    {id: '4', title: 'Offer'},
    {id: '5', title: 'Fast delivery'},
  ];
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <View style={styles.appBar}>
        <View style={{flexDirection: 'row'}}>
          <Icon
            name="list"
            size={20}
            color={'red'}
            style={{paddingHorizontal: 10}}
          />
          <View>
            <Text style={styles.bold_text}>Kazhakkoottam</Text>
            <Text style={{fontSize: 12}}>
              11th Floor, Ganga Tower, Technopark phase 3
            </Text>
          </View>
        </View>
        <View>
          <Text>:</Text>
        </View>
      </View>
      <View style={{padding: 10}}></View>
      <View style={styles.search_container}>
        <Pressable
          onPress={() => navigation.navigate(RoutPath.search as never)}>
          <View style={{flexDirection: 'row'}}>
            <MaterialCommunityIcons
              style={{paddingHorizontal: 10}}
              name="home"
              size={30}
              color="blue"
            />
            <TextInput
              style={styles.input}
              placeholder="Search for dishes and restaurants "
              keyboardType="numeric"
            />
            <View style={styles.partitionLineVertical} />
            <MaterialCommunityIcons
              style={{paddingHorizontal: 10}}
              name="home"
              size={30}
              color="blue"
            />
          </View>
        </Pressable>
        <View style={{padding: 10}}></View>
        <View style={{height: 200, borderRadius: 10, overflow: 'hidden'}}>
          <Image
            source={require('../assets/main.png')}
            style={styles.image}
            resizeMode="cover"
          />
        </View>
        <View style={{padding: 10}}></View>
        <Text style={styles.title_text}>---- Selected from category -----</Text>
        <View style={{padding: 10}}></View>
        <View style={{height: 230, alignSelf: 'center'}}>
          <FlatList
            data={Dishes}
            numColumns={4}
            renderItem={({item}) => (
              <View style={{width: 90}}>
                <View style={styles.circle}>
                  <Image
                    source={require('../assets/icecreame.jpg')}
                    style={styles.image}
                    resizeMode="cover"
                  />
                </View>
                <Text style={{alignSelf: 'center', color: 'black'}}>
                  {item.title}
                </Text>
              </View>
            )}
            keyExtractor={item => item.id}
            //  horizontal={true}
            ItemSeparatorComponent={() => <View style={{padding: 7}}></View>}
          />
        </View>

        <View style={{padding: 10}}></View>
        <Text style={styles.title_text}>---- All restaurants -----</Text>
        <View style={{padding: 13}}></View>
        <View>
          <FlatList
            data={Restaurant}
            renderItem={({item}) => (
              <View style={styles.rectangle}>
                <Text
                  style={{
                    color: 'black',
                    paddingHorizontal: 20,
                    paddingVertical: 5,
                  }}>
                  {item.title}
                </Text>
              </View>
            )}
            keyExtractor={item => item.id}
            horizontal={true}
            ItemSeparatorComponent={() => <View style={{padding: 5}}></View>}
          />
        </View>
      </View>
    </View>
  );
};

export default Delivery;

const styles = StyleSheet.create({
  container: {padding: 20, backgroundColor: 'white', height: '100%'},
  search_container: {
    height: 50,
    backgroundColor: 'white',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 1.5,
    shadowRadius: 7,
    elevation: 5,
  },
  input: {
    height: 50,
    width: '75%',

    padding: 10,

    backgroundColor: 'white',
    color: '#D8D8D8',
  },
  partitionLineVertical: {
    width: 0.7,
    height: '50%',
    backgroundColor: 'black',
    marginHorizontal: 10,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bold_text: {fontWeight: '800', fontSize: 19},
  appBar: {flexDirection: 'row', justifyContent: 'space-between'},
  title_text: {
    alignSelf: 'center',
    color: 'black',
    fontWeight: '400',
    fontSize: 14,
  },
  circle: {
    backgroundColor: 'red',
    height: 80,
    width: 80,
    borderRadius: 40,
    margin: 5,
    overflow: 'hidden',
  },
  image: {height: '100%', width: '100%'},
  rectangle: {
    borderRadius: 5,
    borderWidth: 0.5,
    borderColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
