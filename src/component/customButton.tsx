import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {RoutPath} from '../config/constant';
import {useNavigation} from '@react-navigation/native';

interface ButtonProps {
  title: string;
}

const CustomButton: React.FC<ButtonProps> = ({title}) => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      style={styles.button}
      onPress={() => {
        navigation.navigate(RoutPath.loginPage as never);
      }}>
      <Text style={styles.button_text}>{title}</Text>
    </TouchableOpacity>
  );
};

export default CustomButton;

const styles = StyleSheet.create({
  button: {
    height: 55,
    backgroundColor: '#EE7A57',
    marginTop: 10,
    borderRadius: 5,
  },
  button_text: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 17,
    paddingTop: 13,
  },
});
