import {
  Button,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import { RoutPath } from './config/constant';
import { useNavigation } from '@react-navigation/native';

interface LoginPageProps {}

const LoginPage: React.FC<LoginPageProps> = ({}) => {
   const navigation = useNavigation();
  return (
    <View style={styles.page_body}>
      <View>
        <TouchableOpacity style={styles.skip_button}>
          <Text style={styles.skip_text}>SKIP</Text>
        </TouchableOpacity>
      </View>
      <View>
        <Text style={styles.main_title}>sign in or sign up to Hoppon</Text>
        <Text style={styles.sub_title}>ready when prize drop</Text>
        <TextInput
          style={styles.input}
          placeholder="Username"
          keyboardType="numeric"
        />
        <View style={{padding: 8}}></View>
        <TextInput
          style={styles.input}
          placeholder="Password"
          keyboardType="numeric"
        />
        <View style={{padding: 5}}></View>
        <Text style={{alignSelf: 'flex-end'}}>Forget Password</Text>
        <View style={{padding: 15}}></View>

        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            navigation.navigate(RoutPath.tabNavigationPage as never);
          }}>
          <Text style={styles.button_text}>SIGN IN</Text>
        </TouchableOpacity>
        <View style={{padding: 15}}></View>
        <Text style={{alignSelf: 'center'}}>---- Or continue with -----</Text>
        <View style={{padding: 15}}></View>
        <View style={styles.row}>
          <View style={styles.image_container}>
            <Image
              source={require('../assets/google.png')}
              style={styles.image}
            />
          </View>
          <View style={{padding: 10}}></View>
          <View style={styles.image_container}>
            <Image
              source={require('../assets/apple.png')}
              style={styles.image}
            />
          </View>
          <View style={{padding: 10}}></View>
          <View style={styles.image_container}>
            <Image
              source={require('../assets/facebook.png')}
              style={styles.image}
            />
          </View>
        </View>
        <View style={{padding: 20}}></View>
        <View style={styles.row}>
          <Text style={{color: 'black'}}>Don't we have an account? </Text>

          <Text style={{color: '#EE7A57'}}>Register now</Text>
        </View>
      </View>
      <View style={styles.row}>
        <Text>By login in,you agree to the </Text>
        <Text style={styles.highlight_text}>Terms </Text>
        <Text>and </Text>
        <Text style={styles.highlight_text}>Condition</Text>
      </View>
    </View>
  );
};

export default LoginPage;

const styles = StyleSheet.create({
  input: {
    height: 55,
    width: '100%',

    padding: 10,
    borderRadius: 10,
    backgroundColor: 'white',
    color: '#D8D8D8',
  },
  main_title: {
    // marginHorizontal: 60,
    fontSize: 23,
    color: 'black',
    alignItems: 'center',
    alignSelf: 'center',
  },
  sub_title: {
    alignSelf: 'center',

    fontSize: 13,
    paddingBottom: 40,
  },
  button: {
    height: 55,
    backgroundColor: '#EE7A57',
    marginTop: 10,
    borderRadius: 10,
  },
  button_text: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 17,
    paddingTop: 13,
  },
  row: {flexDirection: 'row', alignSelf: 'center'},
  highlight_text: {color: 'black'},
  skip_button: {
    height: 30,
    width: 55,
    backgroundColor: '#E0E0E0',
    borderRadius: 20,
    alignSelf: 'flex-end',
  },
  skip_text: {paddingTop: 5, color: 'black', alignSelf: 'center'},
  image_container: {
    height: 60,
    width: 60,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: 'white',

    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {height: 30, width: 30},
  page_body: {
    backgroundColor: '#F2EEF1',
    height: '100%',
    justifyContent: 'space-between',
    padding: 20,
  },
});
