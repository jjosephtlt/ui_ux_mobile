import {StyleSheet} from 'react-native';
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {RoutPath} from './config/constant';
import Delivery from './delivery';
import TakeAway from './takeAway';
import Amount from './amount';

interface TabNavigationProps {}

const TabNavigationPage: React.FC<TabNavigationProps> = props => {
  const Tab = createBottomTabNavigator();
  return (
    <Tab.Navigator
      screenOptions={
        {
          // headerTitleAlign: 'center',
          // tabBarActiveTintColor: 'blue',
          // headerTintColor: 'blue',
          // tabBarActiveBackgroundColor: 'blue',
        }
      }>
      <Tab.Screen
        name={RoutPath.deliveryPage}
        component={Delivery}
        options={{
          headerShown: false,
          tabBarLabel: 'Delivery',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="home" color={color} size={22} />
          ),
        }}></Tab.Screen>
      <Tab.Screen
        options={{
          headerShown: false,
          tabBarLabel: 'Take away',
          tabBarIcon: ({color}) => (
            <Icon name="list" size={20} color={color} />
          ),
        }}
        name={RoutPath.takeAway}
        component={TakeAway}></Tab.Screen>
      <Tab.Screen
        options={{
          headerShown: false,
          tabBarLabel: 'Amount',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="account" color={color} size={26} />
          ),
        }}
        name={RoutPath.amount}
        component={Amount}></Tab.Screen>
    </Tab.Navigator>
  );
};

export default TabNavigationPage;

const styles = StyleSheet.create({});
