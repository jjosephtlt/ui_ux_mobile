import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import CustomButton from './component/customButton';
import {RoutPath} from './config/constant';
import {useNavigation} from '@react-navigation/native';

interface LaunchProps {}

const Launch: React.FC<LaunchProps> = ({}) => {
  const navigation = useNavigation();

  return (
    <View style={{padding: 20}}>
      <View style={{height: '72%', overflow: 'hidden', borderRadius: 10}}>
        <Image source={require('../assets/launch1.png')} style={styles.image} />
      </View>
      <View style={{padding: 15}}></View>
      <Text style={styles.bold_text}>LET'S GET</Text>
      <Text style={styles.bold_text}>STARTED</Text>
      <Text style={{fontSize: 12}}>
        Loreim ipsum dolor sit amet, consector adipiscity consector adipiscity
      </Text>
      <View style={{padding: 15}}></View>
      <CustomButton
       
        title="JOIN NOW"></CustomButton>
     
    </View>
  );
};

export default Launch;

const styles = StyleSheet.create({
  bold_text: {fontSize: 20, fontWeight: '900', color: 'black'},
  image: {height: '100%', width: '100%'},
  button: {
    height: 55,
    backgroundColor: '#EE7A57',
    marginTop: 10,
    borderRadius: 5,
  },
  button_text: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 17,
    paddingTop: 13,
  },
});
