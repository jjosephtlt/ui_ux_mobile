import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import LoginPage from './LoginPage';
import {RoutPath} from './config/constant';
import TabNavigationPage from './TabNavigation';
import Launch from './lounch';
import TakeAway from './takeAway';
import SearchPage from './SearchPage';

const Stack = createNativeStackNavigator();

const AppNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={RoutPath.launch}>
        <Stack.Screen
          name={RoutPath.launch}
          component={Launch}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name={RoutPath.loginPage}
          component={LoginPage}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name={RoutPath.tabNavigationPage}
          component={TabNavigationPage}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name={RoutPath.search}
          component={SearchPage}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
