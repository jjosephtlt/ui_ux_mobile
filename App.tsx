/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import type {PropsWithChildren} from 'react';
import {StyleSheet} from 'react-native';

import LoginPage from './src/LoginPage';
import AppNavigator from './src/appNavigator';
import Launch from './src/lounch';

function App(): React.JSX.Element {
  return <AppNavigator></AppNavigator>;
}

const styles = StyleSheet.create({});

export default App;
